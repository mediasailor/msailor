# msailor

![WARNING](https://snipstock.com/assets/cdn/png/03fcc3d00548025c0d46f2281ef57c10.png "WARNING") WARNING: Under construction.

- [info](#info)
- [requirements](#requirements)
- [install](#installation)
- [usage](#usage)
- [plugins](#plugins)

## Info

This is a python script that using external python scripts, can manage almost every kind of media type.

## Requirements

* [git](https://git-scm.com/)
* [mpv](https://mpv.io/) - A free, open source, and cross-platform media player.

- For youtube and soundcloud:
* [yt-dlp](https://github.com/yt-dlp/yt-dlp) - Command-line program to download videos from YouTube.com and other video sites.

- For torrents:
* [peerflix](https://github.com/mafintosh/peerflix) - Streaming torrent client for node.js.

(All requirements are optional, which means that you can install yt-dlp and use the app only for yt and soundcloud)

## Installation

- Windows:
Download and execute the .exe installer.

- Linux:
You must download manually all the dependencies and then curl the [msailor.py](https://gitlab.com/mediasailor/msailor/-/raw/main/msailor.py) script and give it permissions.

This is an example of an installation script (arch linux):
```
pacman -Sy mpv python yt-dlp npm --noconfirm
npm install -g peerflix
curl -sL "https://gitlab.com/mediasailor/msailor/-/raw/main/msailor.py" -o /usr/local/bin/msailor
chmod +x /usr/local/bin/msailor
```

## Update
- The script has an "update" function that will update the script. Your just need to restart the script after that. Keep in mind that this functionality only works on *nix operative systems.

## Usage

For linux:
- All configuration is inside $XDG_CONFIG_HOME/msailor or ~/.config/msailor if $XDG_CONFIG_HOME is not set.
For Windows:
- All configuration is inside %APPDATA%/msailor or

- msailor folder example:
```
.../msailor
   | config (your config file)
   | source (file containing index web pages to scrap from)
   | quickmark (your quickmarks)
   | plug/ (plugins folder)
   | list/ (folder containing your media lists)
   | sync/ (folder that stores your git synced repos)
```

### sync msailor folder to git repo

You can sync your msailor folder to a repo so others can sync it and you got it saved in a git repo. Use the next .gitignore to keep it clean.

.gitignore:
```
/*
!.gitignore
!config
!quickmark
!source
!list/
```

### config

config is the config file of the app. You can sync this file in your msailor repo.

Options available in config:
```
set novideo
set editor=nvim

sync=https://host.com/path/to/the/git
sync=https://host.com/path/to/the/git
sync=https://host.com/path/to/the/git

plug=https://host.com/path/to/the/git
plug=https://host.com/path/to/the/git
plug=https://host.com/path/to/the/git
...
```

## plugins
- If your want to create a plugin. You just need to create a simple script in python that scrap a webpage, filter by the params given by the user, and returns a string with the name and the url/magnet to the media.
- The name of the plugin must be the name of the git repository where it will be allocated. The main module must be called "main.py".
- The main will get 2 params: The API, which is a python class that gives you all the paths form the users and a method that is the menu that the program use to let the user pick between some options.
- You can check an example of a plugin in this [repo](https://gitlab.com/iruzo/youtube), which is a plugin to search in youtube.

## Credits
- I took this idea after saw [Bugswritter's notflix](https://github.com/Bugswriter/notflix), take a look at his code !.
- [BrutalSphere](https://gitlab.com/BrutalSphere) contributed to this project.
