# file management
import shutil
import stat
# general
import subprocess
import os
import re
import sys
from urllib.request import urlopen
from urllib.request import urlretrieve

# paths
if sys.platform == "linux":
    userconfigpath = os.getenv("XDG_CONFIG_HOME")
    userdatapath = os.getenv("XDG_DATA_HOME")
    usertmppath = os.getenv("XDG_CACHE_HOME")
    if userconfigpath is None:
        userconfigpath = os.path.expanduser("~") + os.sep + ".config"
    if userdatapath is None:
        userdatapath = os.path.expanduser("~") + os.sep + ".local" + os.sep + "share"
    if usertmppath is None:
        usertmppath = os.path.expanduser(
            "~") + os.sep + ".cache" + os.sep + "msailor"
if sys.platform == "win32":
    userconfigpath = os.getenv("APPDATA")
    userdatapath = os.getenv("LOCALAPPDATA")
    usertmppath = os.getenv("TMP")
configpath = userconfigpath + os.sep + "msailor"
datapath = userdatapath + os.sep + "msailor"
tmppath = usertmppath + os.sep + "msailor"
historypath = configpath + os.sep + "history"
syncpath = configpath + os.sep + "sync"
listpath = configpath + os.sep + "list"
plugpath = datapath + os.sep + "plug"

if not os.path.exists(configpath):
    os.mkdir(configpath)
if not os.path.exists(datapath):
    os.mkdir(datapath)
if not os.path.exists(usertmppath):
    os.mkdir(usertmppath)


def resource_path(relative_path):
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = ""
    return os.path.join(base_path, relative_path)


# default config
enablevideo = ""
printhelp = True
if sys.platform == "linux":
    editor = "$EDITOR"
    gitpath = "git"
if sys.platform == "win32":
    editor = "notepad"
    gitpath = "git\\cmd\\git.exe"
if os.path.exists(configpath + os.sep + "config"):
    with open(configpath + os.sep + "config", "r") as f:
        for line in f:
            if "set editor=" in line:
                editor = line.split("=")[1].replace("\n", "")
            if "set novideo" in line:
                enablevideo = "--no-video"
            if "set nohelp" in line:
                printhelp = False


class _Getch:
    """Gets a single character from standard input.  Does not echo to the
screen."""

    def __init__(self):
        try:
            self.impl = _GetchWindows()
        except ImportError:
            try:
                self.impl = _GetchMacCarbon()
            except ImportError:
                self.impl = _GetchUnix()
            except AttributeError:
                self.impl = _GetchUnix()

    def __call__(self):
        return self.impl()


class _GetchUnix:
    def __init__(self):
        self = self

    def __call__(self):
        import tty
        import termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch


class _GetchWindows:
    def __init__(self):
        import msvcrt

    def __call__(self):
        import msvcrt
        return msvcrt.getch()


class _GetchMacCarbon:
    """
    A function which returns the current ASCII key that is down;
    if no ASCII key is down, the null string is returned.  The
    page http://www.mactech.com/macintosh-c/chap02-1.html was
    very helpful in figuring out how to do this.
    """

    def __init__(self):
        import Carbon
        Carbon.Evt  # see if it has this (in Unix, it doesn't)

    def __call__(self):
        import Carbon
        if Carbon.Evt.EventAvail(0x0008)[0] == 0:  # 0x0008 is the keyDownMask
            return ''
        else:
            #
            # The event contains the following info:
            # (what,msg,when,where,mod)=Carbon.Evt.GetNextEvent(0x0008)[1]
            #
            # The message (msg) contains the ASCII char which is
            # extracted with the 0x000000FF charCodeMask; this
            # number is converted to an ASCII character with chr() and
            # returned
            #
            (what, msg, when, where, mod) = Carbon.Evt.GetNextEvent(0x0008)[1]
            return chr(msg & 0x000000FF)


getch = _Getch()

# api class with menu and paths that is sended to plugins as first parameter


class Api():

    def __init__(self):
        self = self

    # paths
    configpath = configpath
    tmppath = tmppath
    historypath = historypath
    syncpath = syncpath
    listpath = listpath
    plugpath = plugpath

    def menu(self, menutitle, options, printhelp=True):
        pat = re.compile(r"[A-Za-z0-9]+|:|\.|-|_|/|\\|\[|\]|~")

        keyboardinput = ""
        selected = -1
        while True:
            # generate current selection menu filtered with user input
            current = [content.replace("\n", "") for content in options]
            for filter in keyboardinput.split():
                current = [content.replace(
                    "\n", "") for content in current if filter.lower() in content.lower()]
            if keyboardinput == "":
                current.append("")

            # print in menu the current selected entry
            selected *= (selected < len(current))
            if len(current) != 0:
                current[selected] = " >> " + current[selected]

            # print menu
            print("\n".join(current))

            # print write section
            if printhelp:
                print(
                    "Default hotkeys: [enter] select, [tab] select between current selection, [esc] clean, [Ctrl+c] exit\n")
            print(menutitle + "~$ " + keyboardinput + "|")

            stdin = getch()
            if b"\xe0" == stdin:  # skip escpae sequences
                continue
            if sys.platform == "win32":
                stdin = stdin.decode("utf-8", "replace")

            # clean terminal
            for i in range(os.get_terminal_size()[1]):
                print()

            # validate input
            if "\r" == stdin:  # enter
                if len(current) > 0:
                    if keyboardinput != current[selected].replace(" >> ", ""):
                        keyboardinput = current[selected].replace(
                            " >> ", "").replace("[args]", "")
                    else:
                        return keyboardinput
                else:
                    return keyboardinput
            if "\x1b" == stdin:  # esc
                keyboardinput = ""
                selected = -1
            if "\t" == stdin:  # tab
                selected += (selected < len(current))
                selected -= len(current) * (selected > len(current)-1)
            if "\x7f" == stdin or "\x08" == stdin:  # backspace
                keyboardinput = keyboardinput[:-1]
            if "\x20" == stdin:  # space
                keyboardinput += " "
            if "\x03" == stdin:  # C-c
                quit()
            if re.fullmatch(pat, stdin):
                keyboardinput += stdin
                selected = -1


# print help


def help():
    help = """
        menu:
            :help                   (show help)
            :update     [*nix ONLY] (update app, you need to restart the app manually)
            :sync                   (sync repositories)
            :sync-plug              (sync plugins)
            :push                   (push msailor folder to your repository doing a 'commit -m 'msailor auto push'' and 'push -f')
            :config-edit            (open config in the editor configured in config ($EDITOR by default))
            :history-edit           (open history in the editor configured in config ($EDITOR by default))
            :quickmark-add          (add latest media played to quickmark)
            :quickmark-edit         (open quickmark in the editor configured in config ($EDITOR by default))
            :list-add               (add latest media played to a list)
                                            (it will show your lists so you can choose where to add the media, or you can write a new list to be added)
                                            (WARNING: magnets do not work properly inside lists)

            :list-del               (delete entire list)
            :list-edit              (open list in the editor configured in config ($EDITOR by default))

            :indexwp [arg]          (scrap index web pages from your 'source' file if you want all, do not pass arguments)
            """
    if sys.platform == "linux":
        os.system(("echo \"" + help + "\" | less"))
    if sys.platform == "win32":
        print(help)
        getch()

# sync external repositories


def syncrepos():
    repos = []
    if os.path.exists(configpath + os.sep + "config"):
        with open(configpath + os.sep + "config", "r") as f:
            for line in f:
                if "sync=" in line:
                    repos.append(line.split("sync=")[1].replace("\n", ""))

    if len(repos) > 0:
        if os.path.exists(syncpath):
            shutil.rmtree(syncpath)

        os.mkdir(syncpath)
        for repo in repos:
            p = subprocess.Popen([resource_path(
                gitpath), "clone", repo, syncpath + os.sep + repo.split("/")[3].replace("\n", "")])
            p.wait()

# sync plugins


def syncplug():
    # get all plugins from config
    plugins = []
    if os.path.exists(configpath + os.sep + "config"):
        with open(configpath + os.sep + "config", "r") as f:
            for line in f:
                if "plug=" in line:
                    plugins.append(line.split("plug=")[1].replace("\n", ""))
    # remove all plugins
    if os.path.exists(plugpath):
        shutil.rmtree(plugpath)
    # create plugins path
    os.mkdir(plugpath)
    # download and install all plugins from config
    for repo in plugins:
        pluginname = repo.split("/")[-1]
        p = subprocess.Popen(
            [resource_path(gitpath), "clone", repo, plugpath + os.sep + pluginname])
        p.wait()
        os.chmod(plugpath, stat.S_IWUSR)


# push current repo


def push():
    p = subprocess.Popen(
        [resource_path(gitpath), "-C", configpath, "stage", "."])
    p.wait()
    p = subprocess.Popen([resource_path(gitpath), "-C",
                         configpath, "commit", "-m", "'msailor auto push'"])
    p.wait()
    p = subprocess.Popen([resource_path(gitpath), "-C", configpath, "push"])
    p.wait()

# search over indexwp in all sources


def indexwp(query):
    query = query.lower().strip()

    sources = []
    # adding local sources
    if os.path.exists(configpath + os.sep + "source"):
        with open(configpath + os.sep + "source", "r") as sourcelist:
            for source in sourcelist:
                sources.append(source.replace("\n", ""))
    # adding remote sources
    if os.path.exists(syncpath):
        for syncedrepo in os.listdir(syncpath):
            if os.path.exists(syncpath + os.sep + syncedrepo + os.sep + "source"):
                with open(syncpath + os.sep + syncedrepo + os.sep + "source", "r") as sourcelist:
                    for source in sourcelist:
                        sources.append(source.replace("\n", ""))

    coincidences = []
    pat = re.compile(
        r'(?<=<a href="(?![/]|\?.=.;.=.|\.{2}/))(.+?)(?:">)(.+?)(?=</a>)')
    for source in set(sources):
        with urlopen(source) as response:
            match = pat.findall(response.read().decode("utf-8"))
            for position in match:
                if any(filetype in position[1] for filetype in [".mkv", ".mp3", ".mp4", ".webm", ".avi", ".flv"]):
                    if query is not None and query in position[1].lower():
                        coincidences.append(source + "/" + position[1])
                    else:
                        coincidences.append(source + "/" + position[1])
    return Api().menu("indexwp", coincidences, printhelp)

# plugin execution


def plug(option):
    command = option.split()[1].split(":")[1]
    params = ""
    if len(option.split()) >= 3:
        params = option.split("[plug] :" + command)[1]

    pluginpath = plugpath + os.sep + command
    if(pluginpath not in sys.path):
        sys.path.append(pluginpath)
    import main
    result = main.main(Api(), params)
    del main
    sys.path.remove(pluginpath)
    return result

# exec media in mpv player


def execmedia(titleurl):
    if titleurl is not None:
        try:
            if tmppath in titleurl:
                p = subprocess.Popen([resource_path("mpv"), '--script-opts=ytdl_hook-ytdl_path=' +
                                     resource_path("yt-dlp"), '--playlist=' + titleurl, enablevideo])
                p.wait()
            elif titleurl.startswith("magnet"):
                p = subprocess.Popen(
                    [resource_path("peerflix"), '-k', titleurl.split()[-1], resource_path("mpv")])
                p.wait()
            elif len(titleurl) > 0:
                p = subprocess.Popen([resource_path(
                    "mpv"), '--script-opts=ytdl_hook-ytdl_path=' + resource_path("yt-dlp"), titleurl.split()[-1], enablevideo])
                p.wait()
        except KeyboardInterrupt:
            print("process closed or error, anyway")
        with open(historypath, "a") as f:
            f.write(titleurl + "\n")

# Cyclomatic complexity is over 9000, I know...
# return True or False if it is necessary reload the menu


def execcommand(option):
    if "help" == option:
        help()
        return False
    if "update" == option and sys.platform == "win32":
        os.remove(__file__)
        urlretrieve(
            "https://gitlab.com/mediasailor/msailor/-/raw/main/msailor.py",
            __file__)
        return False
    if "sync-repos" == option:
        syncrepos()
        return True
    if "sync-plugs" == option:
        syncplug()
        return True
    if "push" == option:
        push()
        return False
    if "config-edit" == option:
        os.system((editor + " " + configpath +
                  os.sep + "config").replace("\n", ""))
        return False
    if "history-edit" == option:
        os.system((editor + " " + configpath +
                   os.sep + "history").replace("\n", ""))
        return False

    # quickmark commands
    if "quickmark-add" == option:
        chosen = ""
        with open(historypath) as f:
            chosen = Api().menu("history", f.readlines(), printhelp)
        if chosen != "":
            with open(configpath + os.sep + "quickmark", "a") as f:
                f.write(chosen + "\n")
        return True
    if "quickmark-edit" == option:
        os.system((editor + " " + configpath +
                  os.sep + "quickmark").replace("\n", ""))
        return True

    # list commands
    if os.path.exists(listpath):
        if "list-add" == option:
            chosen = ""
            with open(historypath) as f:
                chosen = Api().menu("choose a history entry to add to a list: ", f.readlines(), printhelp)
            if chosen != "":
                list = Api().menu("choose a list or create a new one: ", os.listdir(listpath), printhelp)
                if list != "":
                    with open(listpath + os.sep + list, "a") as f:
                        f.write(chosen + "\n")
            return True
        if "list-edit" == option:
            list = Api().menu("choose a list to edit: ", os.listdir(listpath), printhelp)
            if list != "":
                os.system((editor + " \"" + configpath + os.sep +
                          "list" + os.sep + list + "\"").replace("\n", ""))
            return False
        if "list-del" == option:
            list = Api().menu("choose a list to delete: ", os.listdir(listpath), printhelp)
            if list != "":
                os.remove(listpath + os.sep + list)
            return True

    if "indexwp" in option:
        execmedia(indexwp(option.split("indexwp")[1]))
        return False

# create array with the menu to display


def menucontent():
    menu_content = []

    if os.path.exists(syncpath):
        for syncedrepo in os.listdir(syncpath):
            # synced lists
            if os.path.exists(syncpath + os.sep + syncedrepo + os.sep + "list"):
                for syncedlist in os.listdir(syncpath + os.sep + syncedrepo + os.sep + "list"):
                    menu_content.append(
                        "[list-" + syncedrepo + "] " + syncedlist)
            # synced quickmarks
            if os.path.exists(syncpath + os.sep + syncedrepo + os.sep + "quickmark"):
                with open(syncpath + os.sep + syncedrepo + os.sep + "quickmark", "r") as quickmarks:
                    for quickmark in quickmarks:
                        menu_content.append(
                            "[quickmark-" + syncedrepo + "] " + quickmark)

    # lists
    if os.path.exists(listpath):
        for list in os.listdir(listpath):
            menu_content.append("[list] " + list)

    # quickmarks
    if os.path.exists(configpath + os.sep + "quickmark"):
        with open(configpath + os.sep + "quickmark", "r") as quickmarks:
            for quickmark in quickmarks:
                menu_content.append("[quickmark] " + quickmark)

    # local files
    if os.path.exists(configpath + os.sep + "file"):
        for file in os.listdir(configpath + os.sep + "file"):
            menu_content.append("[file] " + file)

    # plug
    menu_content.append("")
    if os.path.exists(plugpath):
        for file in os.listdir(plugpath):
            menu_content.append("[plug] :" + file)

    # add options to menu

    menu_content.extend([
        "",
        "[command] help",
    ])
    if sys.platform == "win32":
        menu_content.extend([
            "[command] update",
        ])
    menu_content.extend([
        "[command] sync-repos",
        "[command] sync-plugs",
        "[command] push",
        "[command] config-edit",
        "[command] history-edit",
        "[command] quickmark-add",
        "[command] quickmark-edit",
        "[command] list-add",
        "[command] list-edit",
        "[command] list-del",
        "[command] indexwp",
    ])

    return menu_content


def main():
    content = menucontent()
    while 1:
        option = Api().menu("msailor", content, printhelp)
        if len(option) == 0:
            pass

        if "[command]" in option:
            if execcommand(option.replace("[command] ", "")):
                content = menucontent()
            continue

        # plugins and media execution
        if "[file]" in option:
            execmedia(configpath + os.sep + "file" +
                      os.sep + option.split("[file] ")[1])
            continue
        if "[quickmark" in option:
            if "[quickmark]" in option:
                execmedia(option.split("[quickmark] ")[1])
            else:
                execmedia(option.split("[quickmark-")[1].split("]")[0])
            continue
        if "[list" in option:
            if "[list]" in option:
                file = listpath + os.sep + option.split("[list] ")[1]
            else:
                file = syncpath + os.sep + option.split("[list-")[1].split(
                    "]")[0] + os.sep + "list" + os.sep + option.split("[list] ")[1]
            with open(file, "r") as list:
                media = []
                for item in list:
                    media.append(item.split()[-1] + "\n")
            with open(tmppath, "w") as tmp:
                tmp.writelines(media)
            execmedia(tmppath)
            continue
        if "[plug]" in option:
            execmedia(plug(option))
            content = menucontent()
            continue
        if any(x in option for x in ["http", "https", "magnet", "acestream"]):
            execmedia(option)
            continue


if __name__ == "__main__":
    # clean terminal
    for i in range(os.get_terminal_size()[1]):
        print()
    main()
